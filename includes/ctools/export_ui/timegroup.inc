<?php

/**
 * @file
 * Export UI
 */

$plugin = array(
  // The name of the table as found in the schema in hook_install. This
  // must be an exportable type with the 'export' section defined.
  'schema' => 'timegroup',

  // The access permission to use. If not provided it will default to
  // 'administer site configuration'
  'access' => 'timegroup_administer_timegroups',

  // You can actually define large chunks of the menu system here. Nothing
  // is required here. If you leave out the values, the prefix will default
  // to admin/structure and the item will default to the plugin name.
  'menu' => array(
    'menu prefix' => 'admin/config/development',
    'menu item' => 'timegroup',
    // Title of the top level menu. Note this should not be translated,
    // as the menu system will translate it.
    'menu title' => 'Time groups',
    // Description of the top level menu, which is usually needed for
    // menu items in an administration list. Will be translated
    // by the menu system.
    'menu description' => 'Administer Time Group objects.',
  ),

  // These are required to provide proper strings for referring to the
  // actual type of exportable. "proper" means it will appear at the
  // beginning of a sentence.
  'title singular' => t('TimeGroup'),
  'title singular proper' => t('Time group'),
  'title plural' => t('TimeGroups'),
  'title plural proper' => t('Time groups'),

  // This will provide you with a form for editing the properties on your
  // exportable, with validate and submit handler.
  //
  // The item being edited will be in $form_state['item'].
  //
  // The submit handler is only responsible for moving data from
  // $form_state['values'] to $form_state['item'].
  //
  // All callbacks will accept &$form and &$form_state as arguments.
  'form' => array(
    'settings' => 'timegroup_ctools_export_ui_form',
    'validate' => 'timegroup_ctools_export_ui_form_validate',
    // 'submit' => 'timegroup_ctools_export_ui_form_submit',
  ),
);

/**
 * Edit form for timegroup items.
 *
 * Called by CTools export_ui plugin.
 */
function timegroup_ctools_export_ui_form(&$form, &$form_state) {
  $weight = 0;

  $form['info']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $form_state['item']->title,
    '#weight' => $weight++,
  );

  $form['info']['name']['#type'] = 'machine_name';
  $form['info']['name']['#weight'] = $weight++;
  $form['info']['name']['#machine_name'] = array(
    'source' => array('info', 'title'),
    'exists' => 'timegroup_load',
  );

  $form['info']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $form_state['item']->description,
    '#weight' => $weight++,
  );

  $form['info']['timeoffset'] = array(
    '#type' => 'textfield',
    '#title' => t('Time offset'),
    '#required' => TRUE,
    '#default_value' => $form_state['item']->timeoffset,
    '#maxlength' => 8,
    '#size' => 10,
    '#field_suffix' => t('second'),
    '#element_validate' => array('element_validate_integer'),
    '#weight' => $weight++,
  );

  $form['info']['lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Lifetime'),
    '#required' => TRUE,
    '#default_value' => $form_state['item']->lifetime,
    '#maxlength' => 8,
    '#size' => 10,
    '#field_suffix' => t('second'),
    '#element_validate' => array('element_validate_integer'),
    '#weight' => $weight,
  );

  // To Process a machine_name element only works when
  // the source element is already processed.
  uasort($form['info'], 'element_sort');
}

/**
 * Validate the TimeGroup form submission.
 *
 * Called by CTools export_ui plugin.
 */
function timegroup_ctools_export_ui_form_validate(&$form, &$form_state) {
  if (
    !empty($form_state['values']['timeoffset'])
    &&
    $form_state['values']['timeoffset'] >= $form_state['values']['lifetime']
  ) {
    form_set_error('timeoffset', t('Time offset must be lower than the lifetime.'));
  }
}
