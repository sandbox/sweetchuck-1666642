<?php

/**
 * @file
 * Provide default a TimeGroup.
 */

/**
 * Implements hook_default_timegroup().
 */
function timegroup_default_timegroup() {
  $return = array();

  $return['timegroup_default'] = (object) array(
    'api_version' => TIMEGROUP_API_VERSION_CURRENT,
    'name' => 'timegroup_default',
    'title' => t('Default'),
    'description' => t('Default TimeGroup.'),
    'timeoffset' => 0,
    'lifetime' => 3600,
  );

  $return['timegroup_none'] = (object) array(
    'api_version' => TIMEGROUP_API_VERSION_CURRENT,
    'name' => 'timegroup_none',
    'title' => t('None'),
    'description' => t('Has no lifetime.'),
    'timeoffset' => 0,
    'lifetime' => 0,
  );

  return $return;
}
