<?php

/**
 * @file
 * Views cache plugin
 */

/**
 * Cache plugin for views based on TimeGroup.
 */
class timegroup_views_plugin_cache_timegroup extends views_plugin_cache_time {

  /**
   * Information about options for all kinds of purposes will be held here.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['results_lifespan'] = array('default' => 'timegroup_default');
    $options['output_lifespan'] = array('default' => 'timegroup_default');

    unset($options['results_lifespan_custom'], $options['output_lifespan_custom']);

    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['results_lifespan']['#options']
      = $form['output_lifespan']['#options']
        = timegroup_get_timegroup_options();

    unset($form['results_lifespan_custom'], $form['output_lifespan_custom']);
  }

  /**
   * Validate the options form.
   */
  public function options_validate(&$form, &$form_state) {
    // Suppress the parent validation.
  }

  /**
   * Returns the summary of the settings in the display.
   */
  public function summary_title() {
    $results_tg = timegroup_load($this->options['results_lifespan']);
    $output_tg = timegroup_load($this->options['output_lifespan']);
    $results = $results_tg ? $results_tg->title : t('Broken/missing handler');
    $output = $output_tg ? $output_tg->title : t('Broken/missing handler');
    return check_plain("$results / $output");
  }

  /**
   * Determine the expiration time of the cache type, or NULL if no expire.
   */
  public function cache_expire($type) {
    if (
      ($tg_name = $this->options[$type . '_lifespan'])
      &&
      ($tg = timegroup_load($tg_name))
    ) {
      return REQUEST_TIME + $tg->timeoffset - $tg->lifetime;
    }

    return FALSE;
  }

  /**
   * Determine expiration time in the cache table of the cache type.
   */
  public function cache_set_expire($type) {
    if (
      ($tg_name = $this->options[$type . '_lifespan'])
      &&
      ($tg = timegroup_load($tg_name))
    ) {
      return timegroup_lifetime_expire($tg->timeoffset, $tg->lifetime);
    }

    return CACHE_PERMANENT;
  }
}
