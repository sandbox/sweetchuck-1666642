<?php

/**
 * @file
 * Views integration.
 */

/**
 * Implements hook_views_plugins().
 */
function timegroup_views_plugins() {
  return array(
    'cache' => array(
      'timegroup' => array(
        'title' => t('TimeGroup based'),
        'help' => t('TimeGroup based caching of data.'),
        'handler' => 'timegroup_views_plugin_cache_timegroup',
        'uses options' => TRUE,
        'help topic' => 'cache-time',
      ),
    ),
  );
}
